#pragma once
#pragma once
//#include <iostream>
#include <vector>
#include <stack>
#include <queue>
#include <math.h>



template <typename T>
class BinaryTree
{

private:
	class Node
	{
	public:
		T     Data;
		Node* Left;
		Node* Right;

		Node(const T& Item)
			:Data(Item), Left(nullptr), Right(nullptr) {/**/
		}//default constructor

		Node(const T& Item, Node* L, Node* R) noexcept
			:Data(Item), Left(L), Right(R) {/**/
		}//constructor

		~Node() { Left = Right = nullptr; }//destructor

		//--- AUXILIARY METHODS ---//
		inline bool IsLeafNode() const noexcept
		{
			return Left == nullptr && Right == nullptr;
		}

		inline bool HasOneChild() const noexcept
		{
			return (Left == nullptr && Right != nullptr) || (Left != nullptr && Right == nullptr);
		}

		inline bool HasTwoChildren() const noexcept
		{
			return (Left != nullptr && Right != nullptr);
		}
		//------------------------//

	};//NodeClass

	Node* Root;
	size_t NumNodes;

	//--- PRIVATE METHODS ---//
	inline bool DeleteTree(Node* node)noexcept;
	inline size_t Height(Node* node)const noexcept;
	inline size_t NodeCount(Node* node)const noexcept;
	inline void InOrder(Node* node)const noexcept;
	inline void PostOrder(Node* node)const noexcept;
	inline void PreOrder(Node* node)const noexcept;
	inline void LevelOrder(Node* node)const noexcept;
	inline bool Compare(const Node* a, const Node* b)const noexcept;
	inline typename BinaryTree<T>::Node* DeleteNode(Node* Root, T item)noexcept;
	inline typename BinaryTree<T>::Node* MinValue(Node* Root)const noexcept;
	inline typename BinaryTree<T>::Node* MaxValue(Node* Root)const noexcept;
public:

	//--- PUBLIC METHODS ---//
	BinaryTree()noexcept;//Default Constructor
	~BinaryTree()noexcept;//Destructor
	BinaryTree(const BinaryTree<T>& other) noexcept;//Copy Constructor
	BinaryTree<T>& operator= (const BinaryTree<T>& other)noexcept;//Operator=
	BinaryTree(BinaryTree<T>&& other)noexcept;//Move constructor
	BinaryTree<T>& operator= (const BinaryTree<T>&& other)noexcept;//move operator


	inline bool Add(const T& newData)noexcept;
	inline bool DeleteTree()noexcept;//Delete Tree
	inline bool Compare(const BinaryTree<T>& other) const noexcept;//Compare two trees
	inline bool operator==(const BinaryTree<T>& other) const noexcept;//Compare two trees
	inline bool isEmpty()const noexcept;//Checks if Tree is Empty
	inline bool SearchNode(const T& Value)const noexcept;//Searches if a Node Exists
	//inline bool DeleteNode(const T &data)noexcept;//Deletes a node if it exists
	inline bool DeleteNode(const T& data)noexcept;//Deletes a node if it exists
	inline size_t GetItemCount()const noexcept;//Returns the number of nodes inside the tree
	inline size_t Height()const noexcept;//Returns the height of the tree
	inline size_t NodeCount()const noexcept;// Counts the number of nodes 
	inline void PreOrder()const noexcept;
	inline void PostOrder()const noexcept;
	inline void InOrder()const noexcept;
	inline void LevelOrder()const noexcept;
	inline std::vector<T> ToArray() const noexcept; //from a vector to a BST
	inline bool FromArray(const std::vector<T>& v) noexcept; //from a BST to a vector
	inline T MinValue()const noexcept;
	inline T MaxValue()const noexcept;
	inline bool isFull() const noexcept;
};//BinaryTree

//--- IMPLEMENTATION ---//
template<typename T>
BinaryTree<T>::BinaryTree()noexcept
	:Root(nullptr), NumNodes(0) {/**/
}//Default Constructor

template<typename T>
BinaryTree<T>::~BinaryTree() { DeleteTree(); }//Destructor

template<typename T>
BinaryTree<T>::BinaryTree(const BinaryTree<T>& other)noexcept
{
	if (other.isEmpty())return;
	else
	{
		std::stack<Node*> nodeStack;
		nodeStack.push(other.Root);

		while (nodeStack.empty() == false)
		{
			Node* node = nodeStack.top();
			Add(node->Data);
			nodeStack.pop();

			if (node->Right != nullptr)nodeStack.push(node->Right);
			if (node->Left != nullptr)nodeStack.push(node->Left);

		}//if
	}//if
}//copy constructor

template<typename T>
BinaryTree<T>& BinaryTree<T>::operator=(const BinaryTree<T>& other)noexcept
{
	if (other.isEmpty())return *this;
	else
	{
		std::stack<Node*> nodeStack;
		nodeStack.push(other.Root);

		while (nodeStack.empty() == false)
		{
			Node* node = nodeStack.top();
			Add(node->Data);
			nodeStack.pop();

			if (node->Right != nullptr)nodeStack.push(node->Right);
			if (node->Left != nullptr)nodeStack.push(node->Left);
		}//if
	}//if
	return *this;
}//operator=

template<typename T>
BinaryTree<T>::BinaryTree(BinaryTree<T>&& other)noexcept
	:Root(other.Root), NumNodes(other.NumNodes)
{
	other.Root = nullptr;
	other.NumNodes = 0;
}//move constructor

template<typename T>
BinaryTree<T>& BinaryTree<T>::operator=(const BinaryTree<T>&& other) noexcept
{
	if (this != &other)
	{
		DeleteTree();
		Root = other.Root;
		NumNodes = other.NumNodes;
		other.Root = nullptr;
		other.NumNodes = 0;
	}//if
}//move operator

  ///<summary>
  ///  <para>This method Adds a new node to an existing BinaryTree object</para>
  ///  <para>returns true if is succeeds and false if the node already exists</para> 
  ///  <para>Complexity: Space: O(1) Time:O(logN)</para>
  ///</summary>
template<typename T>
inline bool BinaryTree<T>::Add(const T& newData)noexcept
{
	Node* newNode = new(std::nothrow) Node(newData);//create new node with a value
	if (isEmpty())Root = newNode;
	else
	{
		Node* Parent = nullptr;
		Node* tmp = Root;

		do
		{
			Parent = tmp;
			if (newData > Parent->Data) tmp = tmp->Right;
			else if (newData < Parent->Data) tmp = tmp->Left; //traverse the tree 
			else
			{
				delete newNode;
				return false;//already exists
			}
		} while (tmp != nullptr);

		if (newData > Parent->Data) Parent->Right = newNode; //add the node left or right of the parent node
		else Parent->Left = newNode;


	}//if
	NumNodes++;
	return true;//new node added succesfully
}//Add

template<typename T>
inline bool BinaryTree<T>::DeleteTree(Node* node)noexcept
{
	if (node != nullptr)
	{
		DeleteTree(node->Left); //postorder traversal of the tree
		DeleteTree(node->Right);
		delete node;
		Root = nullptr;
		NumNodes = 0;

		return true;//tree deleted
	}//if
	else return false;//node is nullptr
}//DeleteTree

  ///<summary>
  ///  <para>This method deletes all the nodes from a BinaryTree object</para>
  ///  <para>returns true if it succeeds and false if the we have a null node</para> 
  ///  <para>Complexity: Space: O(1) Time:O(N)</para>
  ///</summary>
template<typename T>
inline bool BinaryTree<T>::DeleteTree()noexcept
{
	if (!isEmpty()) return DeleteTree(Root);
	else return true; //return true beacause its empty 
}//DeleteTree

  ///<summary>
  ///  <para>This method compares two binary search trees </para>
  ///  <para>returns true if the trees are identical and false if they are not</para> 
  ///  <para>Complexity: Space: O(1) Time:Number of nodes in two trees be m and n then complexity of sameTree() is O(m) where m < n.</para>
  ///</summary>
template<typename T>
inline bool BinaryTree<T>::Compare(const BinaryTree<T>& other)const noexcept
{
	if (NumNodes != other.NumNodes)return false;
	else return Compare(Root, other.Root);
}//Compare

template<typename T>
inline bool BinaryTree<T>::Compare(const Node* First, const Node* Second)const noexcept
{
	if (First == nullptr || Second == nullptr) return First == Second;
	if (First->Data != Second->Data) return false;

	return  Compare(First->Left, Second->Left) && Compare(First->Right, Second->Right);
}//Compare

template<typename T>
inline bool BinaryTree<T>::operator==(const BinaryTree<T>& other) const noexcept
{
	if (this->NumNodes != other.NumNodes)return false;
	else return Compare(Root, other.Root);
}//operator==

  ///<summary>
  ///  <para>This method returns true if the tree is empty </para>
  ///  <para>and false if it is not</para> 
  ///  <para>Complexity: Space: O(1) Time:O(1)</para>
  ///</summary>
template<typename T>
inline bool BinaryTree<T>::isEmpty()const noexcept
{
	return(NumNodes == 0);
}//isEmpty

template<typename T>
inline bool BinaryTree<T>::SearchNode(const T& Value)const noexcept
{
	Node* tmp = Root;
	while (tmp != nullptr)
	{
		if (Value > tmp->Data) tmp = tmp->Right;
		else if (Value < tmp->Data) tmp = tmp->Left;
		else return true;
	}//while
	return false;
}//Search Node

template<typename T>
inline bool BinaryTree<T>::DeleteNode(const T& data) noexcept
{
	if (Root == nullptr) return false;
	else return DeleteNode(Root, data);
}//DeleteNode

template<typename T>
inline typename BinaryTree<T>::Node* BinaryTree<T>::DeleteNode(Node* Root, T Item) noexcept
{
	Node* Tmp;
	if (Root == nullptr)
	{
		// Element not found.
		return (nullptr);
	}
	else if (Item < Root->Data)
	{
		// Recurse left.
		Root->Left = DeleteNode(Root->Left, Item);
	}
	else if (Item > Root->Data)
	{
		// Recurse right.
		Root->Right = DeleteNode(Root->Right, Item);
	}
	else if ((Root->Left != nullptr) && (Root->Right != nullptr))
	{
		// Element found and we have two sub-trees.
		Tmp = MinValue(Root->Right); // Replace T with...
		Root->Data = Tmp->Data; // ... its successor
		Root->Right = DeleteNode(Root->Right, Root->Data);
	}
	else
	{
		// Element found and we have one sub-tree.
		Tmp = Root;

		if (Root->Left == nullptr) // Handle 0 children
			Root = Root->Right;
		else if (Root->Right == nullptr)
			Root = Root->Left;

		if (Tmp == this->Root)this->Root = Root;
		free(Tmp); // Release node'
		NumNodes--;


	} // if
	return (Root);
}//delete Node

template<typename T>
inline size_t BinaryTree<T>::GetItemCount()const noexcept
{
	return NumNodes;
}//GetItemCount

template<typename T>
inline size_t BinaryTree<T>::Height()const noexcept
{
	return Height(Root);
}//Height

template<typename T>
inline size_t BinaryTree<T>::Height(Node* node)const noexcept
{
	if (node == nullptr) return 0;

	size_t HeightLeft = Height(node->Left);
	size_t HeightRight = Height(node->Right);

	if (HeightLeft > HeightRight)return ++HeightLeft;
	else return ++HeightRight;
}//Height

template<typename T>
inline size_t BinaryTree<T>::NodeCount()const noexcept
{
	size_t count = 0;
	if (Root != nullptr) count = NodeCount(Root);
	return count;
}//Node Count

template<typename T>
inline size_t BinaryTree<T>::NodeCount(Node* node)const noexcept
{
	size_t count = 1;

	if (node->Left != nullptr) count += NodeCount(node->Left);

	if (node->Right != nullptr)count += NodeCount(node->Right);

	return count;
}//Node Count

template<typename T>
inline void BinaryTree<T>::PreOrder()const noexcept
{
	if (Root != nullptr) PreOrder(Root);
}//PreOrder

template<typename T>
inline void BinaryTree<T>::PreOrder(Node* node)const noexcept
{
	if (node != nullptr)
	{
		std::cout << node->Data << " ";
		PreOrder(node->Left);
		PreOrder(node->Right);
	}//if
}//PreOrder

template<typename T>
inline void BinaryTree<T>::PostOrder()const noexcept
{
	if (Root != nullptr) PostOrder(Root);
}//PostOrder

template<typename T>
inline void BinaryTree<T>::PostOrder(Node* node)const noexcept
{
	if (node != nullptr)
	{
		PostOrder(node->Left);
		PostOrder(node->Right);
		std::cout << node->Data << " ";
	}//if
}//PostOrder

template<typename T>
inline void BinaryTree<T>::InOrder()const noexcept
{
	if (Root != nullptr)
		InOrder(Root);
}//InOrder

template<typename T>
inline void BinaryTree<T>::InOrder(Node* node)const noexcept
{
	if (node != nullptr)
	{
		InOrder(node->Left);
		std::cout << node->Data << " ";
		InOrder(node->Right);
	}//if
}//InOrder

template<typename T>
inline void BinaryTree<T>::LevelOrder() const noexcept
{
	if (!isEmpty()) { LevelOrder(Root); }
}//LevelOrder

template<typename T>
inline void BinaryTree<T>::LevelOrder(Node* node) const noexcept
{
	std::queue<Node*> q;
	Node* temp;
	q.push(Root);
	while (!q.empty()) {
		temp = q.front();
		q.pop();
		std::cout << temp->Data << " ";
		if (temp->Left)
			q.push(temp->Left);
		if (temp->Right)
			q.push(temp->Right);
	}//while
}//LevelOrder

template<typename T>
inline std::vector<T> BinaryTree<T>::ToArray() const noexcept
{
	std::vector<T> ArrayData;
	std::stack<Node*> nodeStack;

	nodeStack.push(Root);

	while (nodeStack.empty() == false)
	{
		Node* node = nodeStack.top();
		ArrayData.push_back(node->Data);

		nodeStack.pop();

		if (node->Right)nodeStack.push(node->Right);
		if (node->Left)nodeStack.push(node->Left);
	}
	return std::vector<T>(ArrayData);
}//ToArray

template<typename T>
inline bool BinaryTree<T>::FromArray(const std::vector<T>& v) noexcept
{
	if (!v.empty())
	{
		for (size_t i = 0; i < v.size(); i++)
		{
			Add(v[i]);
		}//for 
		return true;
	}//if
	return false;
} //FromArray

template<typename T>
inline T BinaryTree<T>::MinValue() const noexcept
{
	if (isEmpty())return false;

	Node* tmp = Root;
	T Min = tmp->Data;

	do
	{
		if (Min > tmp->Data) Min = tmp->Data;
		tmp = tmp->Left;
	} while (tmp != nullptr);

	return Min;
}//MinValue

template<typename T>
inline typename BinaryTree<T>::Node* BinaryTree<T>::MinValue(Node* Root) const noexcept
{
	if (isEmpty())return nullptr;

	Node* tmp = Root;
	Node* Min = tmp;

	do
	{
		if (Min->Data > tmp->Data) Min = tmp;/*= tmp->Data;*/
		tmp = tmp->Left;
	} while (tmp != nullptr);
	//std::cout << Min->Data;
	return Min;
}

template<typename T>
inline T BinaryTree<T>::MaxValue() const noexcept
{
	if (isEmpty())return false;

	Node* tmp = Root;
	T Max = tmp->Data;

	do
	{
		if (Max < tmp->Data) Max = tmp->Data;
		tmp = tmp->Right;
	} while (tmp != nullptr);
	return Max;
} //MaxValue

template<typename T>
inline typename BinaryTree<T>::Node* BinaryTree<T>::MaxValue(Node* Root) const noexcept
{
	if (isEmpty())return nullptr;

	Node* tmp = Root;
	Node* Max = tmp;

	do
	{
		if (Max->Data < tmp->Data) Max = tmp;/* = tmp->Data;*/
		tmp = tmp->Right;
	} while (tmp != nullptr);
	//cout << Max->Data;
	return Max;
} //MaxValue

template<typename T>
inline bool BinaryTree<T>::isFull() const noexcept
{
	return (NumNodes == (pow(2, Height()) - 1));
}//isFull 


//Delete Node iteratively
/*template<typename T>
  inline bool BinaryTree<T>::DeleteNode(const T &data)noexcept
  {
	 if (isEmpty())return false;
	 else
	 {
		 Node *tmp = Root;
		 Node *Parent = Root;
		 Node *temp = Root;

		 do
		 {
			 temp = Parent;
			 Parent = tmp;
			 if (data == tmp->Data)
			 {
				 if (tmp->HasTwoChildren())
				 {
					 if (Parent->Data < Root->Data)
					 {
						 temp = Parent;
						 tmp = Parent->Left;

						 while (tmp->Right != nullptr)
						 {
							 temp = tmp;
							 tmp = tmp->Right;
						 }//while

						 Parent->Data = tmp->Data;

						 if (temp->Left == tmp)
						 {

							 temp->Left = tmp->Left;
						 }
						 else
						 {
							 temp->Right = tmp->Left;
						 }

						 delete tmp;
						 NumNodes--;
						 return true;
					 }
					 else if (Parent->Data >= Root->Data)
					 {
						 temp = Parent;
						 tmp = Parent->Right;

						 while (tmp->Left != nullptr)
						 {
							 temp = tmp;
							 tmp = tmp->Left;
						 }//while

						 Parent->Data = tmp->Data;

						 if (temp->Right == tmp)
						 {

							 temp->Right = tmp->Right;
						 }
						 else
						 {
							 temp->Left = tmp->Right;
						 }
						 delete tmp;
						 NumNodes--;

						 return true;
					 }//if
					 else return false;
				 }//if

				 if (tmp->HasOneChild())
				 {
					 if (temp->Left == tmp)
					 {
						 if (tmp->Left != nullptr)
						 {
							 temp->Left = tmp->Left;
							 delete tmp;

							 NumNodes--;
							 return true;
						 }
						 else if (tmp->Right != nullptr)
						 {
							 temp->Left = tmp->Right;
							 delete tmp;
							 NumNodes--;
							 return true;
						 }//if
						 else return false;
					 }//if
					 else if (temp->Right == tmp)
					 {
						 if (tmp->Left != nullptr)
						 {
							 temp->Right = tmp->Left;
							 delete tmp;
							 NumNodes--;
							 return true;
						 }
						 else if (tmp->Right != nullptr)
						 {
							 temp->Right = tmp->Right;
							 delete tmp;
							 NumNodes--;
							 return true;
						 }//if
						 else return false;
					 }
					 else if (tmp == temp)
					 {
						 if (tmp->Left != nullptr)
						 {
							 Root = tmp->Left;
							 delete tmp;
							 NumNodes--;
							 return true;
						 }
						 else
						 {
							 Root = tmp->Right;
							 delete tmp;
							 NumNodes--;
							 return true;
						 }//if
					 }//if
				 }//if

				 if (tmp->IsLeafNode())
				 {
					 if (temp->Left == tmp)
					 {
						 delete tmp;
						 temp->Left = nullptr;
						 NumNodes--;
						 return true;
					 }
					 else if (temp->Right == tmp)
					 {
						 delete tmp;
						 temp->Right = nullptr;
						 NumNodes--;
						 return true;
					 }
					 else
					 {
						 DeleteTree();
						 return true;
					 }//if
				 }//if
			 }//if
			 else if (data > tmp->Data)tmp = tmp->Right;
			 else tmp = tmp->Left;
		 }while (tmp != nullptr);
	 }//if
	return false;
  }//Delete Node*/