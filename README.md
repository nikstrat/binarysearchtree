# BinarySearchTree

BinarySearchTree is a C++ library and as the name suggests it implements a binary tree using templates which means you can create a binary tree using any data type you like. 


## Usage
import the header file in your project and use it as you like 
```c++
BinaryTree<int> somename, tmp;

somename.Add(10);
somename.DeleteNode(10);
somename = tmp;
etc.
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)